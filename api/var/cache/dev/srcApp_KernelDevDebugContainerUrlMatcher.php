<?php

use Symfony\Component\Routing\Matcher\Dumper\PhpMatcherTrait;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcApp_KernelDevDebugContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    use PhpMatcherTrait;

    public function __construct(RequestContext $context)
    {
        $this->context = $context;
        $this->staticRoutes = array(
            '/create-score' => array(array(array('_route' => 'create-score', '_controller' => 'App\\Controller\\ScoreController::createScore'), null, null, null, false, null)),
            '/update-score' => array(array(array('_route' => 'update-score', '_controller' => 'App\\Controller\\ScoreController::updateScore'), null, null, null, false, null)),
            '/update-name' => array(array(array('_route' => 'update-name', '_controller' => 'App\\Controller\\ScoreController::updateScoreName'), null, null, null, false, null)),
            '/high-score' => array(array(array('_route' => 'high-score', '_controller' => 'App\\Controller\\ScoreController::allHighScore'), null, null, null, false, null)),
        );
        $this->regexpList = array(
            0 => '{^(?'
                    .'|/player\\-score/([^/]++)(*:30)'
                .')(?:/?)$}sDu',
        );
        $this->dynamicRoutes = array(
            30 => array(array(array('_route' => 'player-score', '_controller' => 'App\\Controller\\ScoreController::playerScore'), array('id'), null, null, false, null)),
        );
    }
}
