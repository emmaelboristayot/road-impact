<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Psr\Log\LoggerInterface;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcApp_KernelDevDebugContainerUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    private static $declaredRoutes;
    private $defaultLocale;

    public function __construct(RequestContext $context, LoggerInterface $logger = null, string $defaultLocale = null)
    {
        $this->context = $context;
        $this->logger = $logger;
        $this->defaultLocale = $defaultLocale;
        if (null === self::$declaredRoutes) {
            self::$declaredRoutes = array(
        'create-score' => array(array(), array('_controller' => 'App\\Controller\\ScoreController::createScore'), array(), array(array('text', '/create-score')), array(), array()),
        'update-score' => array(array(), array('_controller' => 'App\\Controller\\ScoreController::updateScore'), array(), array(array('text', '/update-score')), array(), array()),
        'update-name' => array(array(), array('_controller' => 'App\\Controller\\ScoreController::updateScoreName'), array(), array(array('text', '/update-name')), array(), array()),
        'high-score' => array(array(), array('_controller' => 'App\\Controller\\ScoreController::allHighScore'), array(), array(array('text', '/high-score')), array(), array()),
        'player-score' => array(array('id'), array('_controller' => 'App\\Controller\\ScoreController::playerScore'), array(), array(array('variable', '/', '[^/]++', 'id', true), array('text', '/player-score')), array(), array()),
    );
        }
    }

    public function generate($name, $parameters = array(), $referenceType = self::ABSOLUTE_PATH)
    {
        $locale = $parameters['_locale']
            ?? $this->context->getParameter('_locale')
            ?: $this->defaultLocale;

        if (null !== $locale && null !== $name) {
            do {
                if ((self::$declaredRoutes[$name.'.'.$locale][1]['_canonical_route'] ?? null) === $name) {
                    unset($parameters['_locale']);
                    $name .= '.'.$locale;
                    break;
                }
            } while (false !== $locale = strstr($locale, '_', true));
        }

        if (!isset(self::$declaredRoutes[$name])) {
            throw new RouteNotFoundException(sprintf('Unable to generate a URL for the named route "%s" as such route does not exist.', $name));
        }

        list($variables, $defaults, $requirements, $tokens, $hostTokens, $requiredSchemes) = self::$declaredRoutes[$name];

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $referenceType, $hostTokens, $requiredSchemes);
    }
}
