<?php

namespace App\Repository;

use App\Entity\Score;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Score|null find($id, $lockMode = null, $lockVersion = null)
 * @method Score|null findOneBy(array $criteria, array $orderBy = null)
 * @method Score[]    findAll()
 * @method Score[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ScoreRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Score::class);
    }

    /**
     * @return Score[] Returns an array of Score objects with rank
     */
    public function findHighScore()
    {
        $subquery = $this->getEntityManager()->createQuery(
            'SELECT GROUP_CONCAT( s.score
            ORDER BY s.score DESC ) 
            FROM App\Entity\Score s'
        )->execute();

        $query = $this->getEntityManager()->createQuery(
            'SELECT s.id, s.name, s.score, s.date, FIND_IN_SET( s.score, (
                :subquery
                )) AS rank
            FROM App\Entity\Score s'
        )->setParameter('subquery', $subquery);

        // returns an array
        return $query->execute();
    }

    /**
     * @return mixed Returns an Score objects with rank
     */
    public function playerScore($id)
    {
        $subquery = $this->getEntityManager()->createQuery(
            'SELECT GROUP_CONCAT( s.score
            ORDER BY s.score DESC ) 
            FROM App\Entity\Score s'
        )->execute();

        $query = $this->getEntityManager()->createQuery(
            'SELECT s.id, s.name, s.score, s.date, FIND_IN_SET( s.score, (
                :subquery
                )) AS rank
            FROM App\Entity\Score s
            WHERE s.id = :id'
        )->setParameter('subquery', $subquery)
        ->setParameter('id', $id);

        // returns an objet
        return $query->getOneOrNullResult();
    }

    /**
     * findOneByName
     *
     * @param  mixed $value
     * @return Score
     */
    public function findOneByName($value): ?Score
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.name = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    
}
