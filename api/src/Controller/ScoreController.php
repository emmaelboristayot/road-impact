<?php

namespace App\Controller;

use App\Entity\Score;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ScoreController extends AbstractController
{

    /**
     * @Route("/high-score", name="high-score")
     */
    public function allHighScore()
    {
        $scores = $this->getDoctrine()
        ->getRepository(Score::class)
        ->findHighScore();

        return $this->json($scores);
    }

    /**
     * @Route("/player-score/{id}", name="player-score")
     */
    public function playerScore(int $id)
    {
        $score = $this->getDoctrine()
        ->getRepository(Score::class)
        ->playerScore($id);

        return $this->json($score);
    }

    /**
     * @Route("/create-score", name="create-score")
     */
    public function createScore(Request $request, ValidatorInterface $validator)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $data = json_decode($request->getContent());

        // var_dump($request->getContent());die;

        $score = new Score();
        $score->setName($data->name);
        $score->setScore($data->score);
        $score->setDate(new \DateTime());

        $errors = $validator->validate($score);

        if (count($errors) > 0) {
            return new Response((string) $errors);
        }

        $entityManager->persist($score);
        $entityManager->flush();

        // var_dump($score);die;
        $score = $this->getDoctrine()
        ->getRepository(Score::class)
        ->playerScore($score->getId());

        return $this->json($score);
    }

    /**
     * @Route("/update-score", name="update-score")
     */
    public function updateScore(int $id, int $score)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $score = $this->getDoctrine()
        ->getRepository(Score::class)
        ->find($id);

        $score->setScore($score);
        $score->setDate(new DateTime());

        $entityManager->flush();

        return $this->json($score);
    }

    /**
     * @Route("/update-name", name="update-name")
     */
    public function updateScoreName(int $id, string $name)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $score = $this->getDoctrine()
        ->getRepository(Score::class)
        ->find($id);

        $score->setName($name);

        $entityManager->flush();

        return new Response('score updated with new name '.$score->getName());
    }
}
