﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Text;

public class uiManager : MonoBehaviour
{
    public Button[] buttons;
    public Text scoreText;
    public Text highScoreText;
    public Text rankText;

    bool isGameOver;
    int score;
    int highScore;
    PlayerScore playerScore;

    private void Awake()
    {
        LoadHighScore();
    }

    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        InvokeRepeating("ScoreUpdate", 1.0f, 0.5f);
        StartCoroutine(ApiGetScore());
        //StartCoroutine(ApiCreateScore());
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = "Score: " + score;
        if (score > highScore)
        {
            highScoreText.text = "High score: " + score;
        }
    }

    void ScoreUpdate() {
        if (!isGameOver) {
            score += 1;
        }
    }

    public void GameOverActivated() {
        isGameOver = true;
        foreach (Button button in buttons) {
            button.gameObject.SetActive(true);
        }
        SaveHighScore();
    }

    public void Play() {
        SceneManager.LoadScene("SampleScene");

    }

    public void Pause()
    {
        if ((int)Time.timeScale == 1) {
            Time.timeScale = 0;
        }
        else if ((int)Time.timeScale == 0)
        {
            Time.timeScale = 1;
        }
    }

    public void Menu () {
        SceneManager.LoadScene("MenuScene");
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void SaveHighScore()
    {
        if (score > highScore) {
            //save in device
            PlayerPrefs.SetInt("HighScore", score);
            //save in server
            if (playerScore == null) {
                StartCoroutine(ApiCreateScore());
            }

        }
    }

    public void LoadHighScore()
    {
        if (PlayerPrefs.HasKey("HighScore")) {
            highScore = PlayerPrefs.GetInt("HighScore", 0);
        }
        highScoreText.text = "High score: " + highScore.ToString();

    }

    IEnumerator ApiGetScore()
    {
        if (playerScore != null) {
            UnityWebRequest www = UnityWebRequest.Get("http://localhost:8888/road-impact/api/public/index.php/player-score/1");
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                playerScore = JsonUtility.FromJson<PlayerScore>(www.downloadHandler.text);
                rankText.text = "World rank: " + playerScore.rank.ToString();
                Debug.Log(playerScore.date);
            }
        }
       
    }

    IEnumerator ApiCreateScore()
    {
        playerScore = new PlayerScore
        {
            name = "tayot",
            score = score
        };

        byte[] bytes = Encoding.UTF8.GetBytes(JsonUtility.ToJson(playerScore));
        UnityWebRequest www = UnityWebRequest.Put("http://localhost:8888/road-impact/api/public/index.php/create-score", bytes);
        www.uploadHandler.contentType = "application/json";
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
            playerScore = JsonUtility.FromJson<PlayerScore>(www.downloadHandler.text);
        }
    }

}
