﻿using System;

[Serializable]
public class PlayerScore
{
    public int id;
    public string name;
    public int score;
    public DateTime date;
    public int rank;
}
