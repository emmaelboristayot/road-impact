﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSpawner : MonoBehaviour {

    public GameObject[] cars;
    public float[] maxPos;
    public float delayTimer = 1f;
    float timer;
    int carNumber;

    // Use this for initialization
    void Start () {
        timer = delayTimer;
	}
	
	// Update is called once per frame
	void Update () {

        timer -= Time.deltaTime;

        if (timer <= 0) {

            Vector3 carPos = new Vector3(maxPos[Random.Range(0, maxPos.Length)], transform.position.y, transform.position.z);

            carNumber = Random.Range(0, cars.Length);
            Instantiate(cars[carNumber], carPos, transform.rotation);
            timer = delayTimer;
        }

    }
}
