﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class carController : MonoBehaviour
{

    public float carSpeed;

    public float leftPos = -1.48f;
    public float middlePos = 0.06f;
    public float rightPos = 1.56f;

    //[-1, 0, 1]
    private int carPos = 0;

    //[-1, 0, 1]
    private float carMoving = 0.0f;

    private int trackLimit = 1;

    public uiManager ui;

    Rigidbody2D rb;

    public Animator animator;

    bool isPlateformAndroid = false;

    private void Awake()
    {
        #if UNITY_ANDROID
                isPlateformAndroid = true;
        #else
                 isPlateformAndroid = false;
        #endif

        rb = GetComponent<Rigidbody2D>();
    }

    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        if (isPlateformAndroid)
        {
            //android specific code
            TouchMove();
        }

        CarMove();

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy Car") {
            gameObject.SetActive(false);
            ui.GameOverActivated();
        }
    }

    public void TouchMove() {
        if (Input.touchCount > 0) {
            Touch touch = Input.GetTouch(0);
            float middle = Screen.width / 2;
            if (touch.position.x < middle && touch.phase == TouchPhase.Began) {
                MoveLeft();
            }

            else if (touch.position.x > middle && touch.phase == TouchPhase.Began)
            {
                MoveRight();
            }
        }

    }

    public void MoveLeft()
    {
        carPos--;
        carMoving = 0.5f;
    }

    public void MoveRight()
    {
        carPos++;
        carMoving = 1.5f;
    }

    private void CarMove () {
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(GetPos(), transform.position.y, 0), carSpeed * Time.deltaTime);

        if (rb.IsSleeping()) {
            carMoving = 0.5f;
        }

        animator.SetFloat("Position", carMoving);
    }

    private float GetPos() {
        carPos = Mathf.Clamp(carPos, -trackLimit, trackLimit);

        if (carPos == -1) {
            return leftPos;
        } else if (carPos == 1) {
            return rightPos;
        }
        else {
            return middlePos;
        }

    }

}
